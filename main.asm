%include "lib.inc"
%include "words.inc"
%include "dict.inc"
%define MAX_BUFFER_SIZE 256

section .rodata
buffer_overflow_message:
	db "Input string exceeded max length.", `\n`, 0
key_not_found_message:
	db "Dictionary doesn't contain specified key.", `\n`, 0
value_found_message:
	db "Found value: ", 0
value_found_message_end:
	db '.', `\n`, 0

section .bss
	buffer: resb MAX_BUFFER_SIZE

section .text

global _start

_start:
	.first_check:
		mov rdi, buffer
		mov rsi, MAX_BUFFER_SIZE
		call read_word
		cmp rax, 0
		jne .second_check
		mov rdi, buffer_overflow_message
		jmp .print_error
	.second_check:
		mov rdi, rax
		mov rsi, first_word
		push rdx
		call find_word
		cmp rax, 0
		jne .print_value
		mov rdi, key_not_found_message
	.print_error:
		call print_error
		mov rdi, 1
		jmp exit
	.print_value:
		push rax
		mov rdi, value_found_message
		call print_string
		pop rax
		pop rdx
		add rax, rdx
		inc rax
		mov rdi, rax
		call print_string
		mov rdi, value_found_message_end
		call print_string
		xor rdi, rdi
		jmp exit
